
///
/// Methods for flickr.activity.*
///

use super::*;
use super::super::*;

// ---- Builder -----------------------------------------------------------------------------------

/// Builder for flickr.activity.* methods.
pub struct Builder<'a> {
    flickr: &'a mut FlickrAPI,
}

impl<'a> Builder<'a> {
    pub fn new(flickr: &mut FlickrAPI) -> Builder {
        Builder {
            flickr: flickr,
        }
    }

    /// Builder for method flickr.activity.userComments.
    pub fn user_comments(&mut self) -> UserCommentsBuilder {
        UserCommentsBuilder::new(self.flickr)
    }

    /// Builder for method flickr.activity.getContext.
    pub fn user_photos(&mut self) -> UserPhotosBuilder {
        UserPhotosBuilder::new(self.flickr)
    }
}

// ---- UserCommentsBuilder -----------------------------------------------------------------------

/// Builder for flickr.activity.userComments.
#[derive(Builder)]
#[method = "flickr.activity.userComments"]
#[result = "UserCommentsResult"]
pub struct UserCommentsBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.activity.userComments.
#[derive(Deserialize, Debug)]
pub struct UserCommentsResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,

    #[serde(default)]
    pub items: Items,
}

// ---- UserPhotosBuilder -------------------------------------------------------------------------

/// Builder for flickr.activity.userPhotos.
#[derive(Builder)]
#[method = "flickr.activity.userPhotos"]
#[result = "UserPhotosResult"]
pub struct UserPhotosBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    timeframe: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

/// Result from flickr.activity.userPhotos.
#[derive(Deserialize, Debug)]
pub struct UserPhotosResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,

    #[serde(default)]
    pub items: Items,
}

// Aggregate of `UserPhotosResult` or `UserCommentsResult`.
#[derive(Deserialize, Debug, Default)]
pub struct Items {
    #[serde(default)]
    pub item: Vec<Item>,
    #[serde(default)]
    pub page: i32,
    #[serde(default)]
    pub pages: i32,
    #[serde(default)]
    pub per_page: i32,
    #[serde(default)]
    pub total: i32,
}

// Aggregate of `Items`.
#[derive(Deserialize, Debug)]
pub struct Item {
    #[serde(default)]
    pub r#type: String,
    #[serde(default)]
    pub id: String,
    #[serde(default)]
    pub owner: String,
    #[serde(default)]
    pub ownername: String,
    #[serde(default)]
    pub realname: String,
    #[serde(default)]
    pub iconserver: String,
    #[serde(default)]
    pub iconfarm: i32,
    #[serde(default)]
    pub secret: String,
    #[serde(default)]
    pub server: String,
    #[serde(default)]
    pub farm: i32,
    #[serde(deserialize_with = "string_from_content_block", default)]
    pub title: String,
    #[serde(default)]
    pub media: String,
    #[serde(default)]
    pub comments: i32,
    #[serde(default)]
    pub notes: i32,
    #[serde(default)]
    pub views: i32,
    #[serde(default)]
    pub faves: i32,
    #[serde(default)]
    pub activity: Events,
}

// Aggregate of `Item`.
#[derive(Deserialize, Debug, Default)]
pub struct Events {
    #[serde(default)]
    pub event: Vec<Event>,
}

// Aggregate of `Events`.
#[derive(Deserialize, Debug)]
pub struct Event {
    #[serde(default)]
    r#type: String,
    #[serde(default)]
    user: String,
    #[serde(default)]
    username: String,
    #[serde(default)]
    iconserver: String,
    #[serde(default)]
    iconfarm: i32,
    #[serde(deserialize_with = "date_from_string")]
    dateadded: DateTime<Local>,
    #[serde(default)]
    is_muted: Option<bool>,
    #[serde(default)]
    realname: String,
    #[serde(default)]
    commentid: String,
    #[serde(default, rename = "_content")]
    content: String,
}

// ---- tests -------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    fn setup(expected_response: &str) -> FlickrAPI {
        let mut flickr = FlickrAPI::new("", "");
        flickr.test_response = Some(expected_response.into());
        return flickr;
    }

    #[test]
    fn user_comments() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
        
        let r = flickr.activity().user_comments().perform().unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn user_photos() {
        let mut flickr = setup(r#"
            {
                "items": {
                    "item": [
                        {
                            "type": "photo",
                            "id": "item_id",
                            "owner": "item_owner",
                            "ownername": "item_ownername",
                            "realname": "item_realname",
                            "iconserver": "item_iconserver",
                            "iconfarm": 6,
                            "secret": "icon_secret",
                            "server": "icon_server",
                            "farm": 5,
                            "title": {
                                "_content": "icon_title"
                            },
                            "media": "icon_media",
                            "comments": 10,
                            "notes": 0,
                            "views": 224,
                            "faves": 6,
                            "activity": {
                                "event": [
                                    {
                                        "type": "fave",
                                        "user": "event_user",
                                        "username": "event_username",
                                        "iconserver": "event_iconserver",
                                        "iconfarm": 4,
                                        "dateadded": "1543890964",
                                        "is_muted": null,
                                        "realname": "event_realname"
                                    },
                                    {
                                        "type": "fave",
                                        "user": "b1",
                                        "username": "b3",
                                        "iconserver": "4",
                                        "iconfarm": 5,
                                        "dateadded": "1543890965",
                                        "is_muted": null,
                                        "realname": "b7"
                                    }
                                ]
                            }
                        }
                   ],
                    "page": 1,
                    "pages": 1,
                    "perpage": 10,
                    "total": 1
                },
                "stat": "ok"
            }
        "#);
        
        let r = flickr.activity().user_photos().perform().unwrap();
        assert_eq!(r.stat, "ok");
        assert_eq!(r.items.item.get(0).unwrap().r#type, "photo");
        assert_eq!(r.items.item.get(0).unwrap().id, "item_id");
        assert_eq!(r.items.item.get(0).unwrap().owner, "item_owner");
        assert_eq!(r.items.item.get(0).unwrap().ownername, "item_ownername");
        assert_eq!(r.items.item.get(0).unwrap().realname, "item_realname");
        assert_eq!(r.items.item.get(0).unwrap().iconserver, "item_iconserver");
        assert_eq!(r.items.item.get(0).unwrap().iconfarm, 6);
        assert_eq!(r.items.item.get(0).unwrap().secret, "icon_secret");
        assert_eq!(r.items.item.get(0).unwrap().server, "icon_server");
        assert_eq!(r.items.item.get(0).unwrap().farm, 5);
        assert_eq!(r.items.item.get(0).unwrap().title, "icon_title");
        assert_eq!(r.items.item.get(0).unwrap().media, "icon_media");
        assert_eq!(r.items.item.get(0).unwrap().comments, 10);
        assert_eq!(r.items.item.get(0).unwrap().notes, 0);
        assert_eq!(r.items.item.get(0).unwrap().views, 224);
        assert_eq!(r.items.item.get(0).unwrap().faves, 6);
        assert_eq!(r.items.item.get(0).unwrap().activity.event.len(), 2);
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().r#type, "fave");
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().user, "event_user");
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().username, "event_username");
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().iconserver, "event_iconserver");
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().iconfarm, 4);
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().dateadded, Local.ymd(2018, 12, 4).and_hms(4, 36, 4));
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().is_muted, None);
        assert_eq!(r.items.item.get(0).unwrap().activity.event.get(0).unwrap().realname, "event_realname");
    }

}
