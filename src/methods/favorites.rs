
///
/// Methods for flickr.favorites.*
///

use super::*;
use super::super::*;

// ---- Builder -----------------------------------------------------------------------------------

/// Builder for flickr.favorites.* methods.
pub struct Builder<'a> {
    flickr: &'a mut FlickrAPI,
}

impl<'a> Builder<'a> {
    pub fn new(flickr: &mut FlickrAPI) -> Builder {
        Builder {
            flickr: flickr,
        }
    }

    /// Builder for method flickr.favorites.getList.
    pub fn get_list(&mut self) -> GetListBuilder {
        GetListBuilder::new(self.flickr)
    }

    /// Builder for method flickr.favorites.getContext.
    pub fn get_context(&mut self) -> GetContextBuilder {
        GetContextBuilder::new(self.flickr)
    }

    /// Builder for method flickr.favorites.Add.
    pub fn add(&mut self) -> AddBuilder {
        AddBuilder::new(self.flickr)
    }

    /// Builder for method flickr.favorites.Remove.
    pub fn remove(&mut self) -> RemoveBuilder {
        RemoveBuilder::new(self.flickr)
    }

    /// Builder for method flickr.favorites.getPublicList.
    pub fn get_public_list(&mut self) -> GetPublicListBuilder {
        GetPublicListBuilder::new(self.flickr)
    }

}

// ---- GetListBuilder ----------------------------------------------------------------------------

/// Builder for flickr.favorites.getList.
#[derive(Builder)]
#[method = "flickr.favorites.getList"]
#[result = "GetListResult"]
pub struct GetListBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    user_id: Option<&'a str>,
    min_fave_date: Option<DateTime<Local>>,
    max_fave_date: Option<DateTime<Local>>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

#[derive(Deserialize, Debug)]
pub struct GetListResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,

    #[serde(default)]
    pub photos: Option<Photos>,
}

#[derive(Deserialize, Debug)]
pub struct Photos {
    #[serde(default)]
    pub page: i32,

    #[serde(default)]
    pub pages: i32,

    #[serde(default)]
    pub perpage: i32,

    #[serde(deserialize_with = "i32_from_string", default)]
    pub total: i32,

    #[serde(default)]
    pub photo: Vec<Photo>,
}

#[derive(Deserialize, Debug)]
pub struct Photo {
    pub id: String,
    pub owner: String,
    #[serde(default)]
    pub secret: String,
    #[serde(default)]
    pub server: String,
    #[serde(default)]
    pub farm: i32,
    #[serde(default)]
    pub title: String,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub ispublic: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfriend: bool,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub isfamily: bool,
    #[serde(deserialize_with = "date_from_string")]
    pub date_faved: DateTime<Local>,
}

// ---- GetContextBuilder -------------------------------------------------------------------------

/// Builder for flickr.favorites.getContext.
#[derive(Builder)]
#[method = "flickr.favorites.getContext"]
#[result = "GetContextResult"]
pub struct GetContextBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
    user_id: &'a str,
}

#[derive(Deserialize, Debug)]
pub struct GetContextResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,

    #[serde(deserialize_with = "i32_from_content_block", default)]
    pub count: i32,

    #[serde(default)]
    pub prevphoto: Option<ContextPhoto>,

    #[serde(default)]
    pub nextphoto: Option<ContextPhoto>,
}

#[derive(Deserialize, Debug)]
pub struct ContextPhoto {
    pub id: String,
    pub owner: String,
    #[serde(default)]
    pub secret: String,
    #[serde(default)]
    pub server: String,
    #[serde(default)]
    pub farm: i32,
    #[serde(default)]
    pub title: String,
    #[serde(default)]
    pub url: String,
    #[serde(default)]
    pub thumb: String,
    #[serde(deserialize_with = "i32_from_string", default)]
    pub license: i32,
    #[serde(default)]
    pub media: String,
    #[serde(deserialize_with = "bool_from_int", default)]
    pub is_faved: bool,
}

// ---- AddBuilder --------------------------------------------------------------------------------

/// Builder for flickr.favorites.add.
#[derive(Builder)]
#[method = "flickr.favorites.add"]
#[result = "AddResult"]
pub struct AddBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: &'a str,
}

#[derive(Deserialize, Debug)]
pub struct AddResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,
}

// ---- RemoveBuilder -----------------------------------------------------------------------------

/// Builder for flickr.favorites.remove.
#[derive(Builder)]
#[method = "flickr.favorites.remove"]
#[result = "RemoveResult"]
pub struct RemoveBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    photo_id: String,
}

#[derive(Deserialize, Debug)]
pub struct RemoveResult {
    #[serde(default)]
    pub stat: String,

    #[serde(default)]
    pub code: i32,

    #[serde(default)]
    pub message: String,

    #[serde(default)]
    pub xxx: String,
}

// ---- GetPublicListBuilder ----------------------------------------------------------------------

/// Builder for flickr.favorites.getPublicList.
#[derive(Builder)]
#[method = "flickr.favorites.getPublicList"]
#[result = "GetListResult"]
pub struct GetPublicListBuilder<'a> {
    flickr: &'a mut FlickrAPI,

    user_id: Option<&'a str>,
    min_fave_date: Option<DateTime<Local>>,
    max_fave_date: Option<DateTime<Local>>,
    extras: Option<&'a str>,
    per_page: Option<i32>,
    page: Option<i32>,
}

// ---- tests -------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    fn setup(expected_response: &str) -> FlickrAPI {
        let mut flickr = FlickrAPI::new("", "");
        flickr.test_response = Some(expected_response.into());
        return flickr;
    }

    #[test]
    fn get_lists() {
        let mut flickr = setup(r#"
            {
                "photos": {
                    "page":1,
                    "pages":2,
                    "perpage":3,
                    "total":"4",
                    "photo": [
                        {
                            "id":"a_id",
                            "owner":"a_owner",
                            "secret":"a_secret",
                            "server":"a_server",
                            "farm":5,
                            "title":"a_title",
                            "ispublic":1,
                            "isfriend":1,
                            "isfamily":1,
                            "date_faved":"222222"
                        },      
                        {
                            "id":"b_id",
                            "owner":"b_owner",
                            "secret":"b_secret",
                            "server":"b_server",
                            "farm":4,
                            "title":"b_title",
                            "ispublic":0,
                            "isfriend":0,
                            "isfamily":0,
                            "date_faved":"333333"
                        }
                    ]
                },
                "stat":"ok"
            }
        "#);
        
        // Test get_list
        let r = flickr.favorites().get_list().perform().unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.pages, 2);
            assert_eq!(photos.perpage, 3);
            assert_eq!(photos.total, 4);
            assert_eq!(photos.photo.len(), 2);
            assert_eq!(photos.photo[0].id, "a_id");
            assert_eq!(photos.photo[0].owner, "a_owner");
            assert_eq!(photos.photo[0].secret, "a_secret");
            assert_eq!(photos.photo[0].server, "a_server");
            assert_eq!(photos.photo[0].farm, 5);
            assert_eq!(photos.photo[0].title, "a_title");
            assert_eq!(photos.photo[0].ispublic, true);
            assert_eq!(photos.photo[0].isfriend, true);
            assert_eq!(photos.photo[0].isfamily, true);
            assert_eq!(photos.photo[0].date_faved, Utc.timestamp(222222, 0));
            assert_eq!(photos.photo[1].id, "b_id");
            assert_eq!(photos.photo[1].owner, "b_owner");
            assert_eq!(photos.photo[1].secret, "b_secret");
            assert_eq!(photos.photo[1].server, "b_server");
            assert_eq!(photos.photo[1].farm, 4);
            assert_eq!(photos.photo[1].title, "b_title");
            assert_eq!(photos.photo[1].ispublic, false);
            assert_eq!(photos.photo[1].isfriend, false);
            assert_eq!(photos.photo[1].isfamily, false);
            assert_eq!(photos.photo[1].date_faved, Utc.timestamp(333333, 0));
        } else {
            assert!(false);
        }

        // Test get_public_list
        let r = flickr.favorites().get_public_list().perform().unwrap();
        assert_eq!(r.stat, "ok");
        if let Some(photos) = r.photos {
            assert_eq!(photos.page, 1);
            assert_eq!(photos.photo.len(), 2);
            assert_eq!(photos.photo[0].id, "a_id");
        } else {
            assert!(false);
        }
    }

    #[test]
    fn get_context() {
        let mut flickr = setup(r#"
            {
                "count": {
                    "_content": "1"
                },
                "prevphoto": {
                    "id": "p_id",
                    "owner": "p_owner",
                    "secret": "p_secret",
                    "server": "2",
                    "farm": 3,
                    "title": "p_title",
                    "url": "p_url",
                    "thumb": "p_thumb",
                    "license": "4",
                    "media": "p_photo",
                    "is_faved": 1
                },
                "nextphoto": {
                    "id": "n_id",
                    "owner": "n_owner",
                    "secret": "n_secret",
                    "server": "6",
                    "farm": 7,
                    "title": "n_title",
                    "url": "n_url",
                    "thumb": "n_thumb",
                    "license": "8",
                    "media": "n_photo",
                    "is_faved": 0
                },
                "stat": "ok"
            }
        "#);
        
        // Test get_context
        let r = flickr.favorites().get_context().perform().unwrap();
        assert_eq!(r.stat, "ok");
        assert_eq!(r.count, 1);

        if let Some(p) = r.prevphoto {
            assert_eq!(p.id, "p_id");
            assert_eq!(p.owner, "p_owner");
            assert_eq!(p.secret, "p_secret");
            assert_eq!(p.server, "2");
            assert_eq!(p.farm, 3);
            assert_eq!(p.title, "p_title");
            assert_eq!(p.url, "p_url");
            assert_eq!(p.thumb, "p_thumb");
            assert_eq!(p.license, 4);
            assert_eq!(p.media, "p_photo");
            assert_eq!(p.is_faved, true);
        } else {
            assert!(false);
        }

        if let Some(p) = r.nextphoto {
            assert_eq!(p.id, "n_id");
            assert_eq!(p.owner, "n_owner");
            assert_eq!(p.secret, "n_secret");
            assert_eq!(p.server, "6");
            assert_eq!(p.farm, 7);
            assert_eq!(p.title, "n_title");
            assert_eq!(p.url, "n_url");
            assert_eq!(p.thumb, "n_thumb");
            assert_eq!(p.license, 8);
            assert_eq!(p.media, "n_photo");
            assert_eq!(p.is_faved, false);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn add() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
        
        // Test get_context
        let r = flickr.favorites().add().perform().unwrap();
        assert_eq!(r.stat, "ok");
    }

    #[test]
    fn remove() {
        let mut flickr = setup(r#"
            {"stat":"ok"}
        "#);
        
        // Test get_context
        let r = flickr.favorites().remove().perform().unwrap();
        assert_eq!(r.stat, "ok");
    }
}

