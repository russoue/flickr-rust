# Flickr-Rust

Flickr-Rust is an unofficial Rust binding to [Flickr API].

## Example

```rust
use flickr::{FlickrAPI};

let mut flickr = FlickrAPI::new(
                  "MY-FLICKR-API-KEY", 
                  "MY-FLICKR-API-SECRET");

let res = flickr.favorites()
                .get_list()
                .perform()?;

println!("{:#?}", res.photos.unwrap());
```

You have to get your custom Flickr API key and Flickr API secret to use this crate.

## Status

The plan is to cover all the essential non-deprecated methods of Flickr API. 
The current method sets coverage is shown below.

**Implemented**: activity, auth.oauth, favorites, photos

**Unimplemented**: blogs, cameras, collections, commons, contacts, galleries, groups, groups.discuss.replies, groups.discuss.topics, groups.members, groups.pools, interestingness, machinetags, panda, people, photos.comments, photos.geo, photos.licenses, photos.notes, photos.people, photos.suggestions, photos.transform, photos.upload, photosets, photosets.comments, places, prefs, profile, push, reflection, stats, tags, test, testimonials, urls

**Deprecated**: auth

In addition, _upload_ and _replace_ features will be implemented too.

See [change log](CHANGELOG.md) for information about version history.

## License

Flickr-Rust is licensed under [Apache License, Version 2.0](LICENSE).

[Flickr API]: https://www.flickr.com/services/api/

